package com.alkhr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Check with correct input
 */

@RunWith(Parameterized.class)
public class SortingWorkingCaseTest {
    String input, expected;
    public SortingWorkingCaseTest(String input, String expected) {
        this.input=input;
        this.expected=expected;
    }
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        String[] inArr = new String[2];
        String inp;
        int run=5;
        Object[][] check = new Object[run][2];
        try {
            URL url = SortingInvalidInputCaseTest.class.getClass().getResource("/testInputNormalCase.txt");
            File inputFile = new File(url.getPath());
//            File inputFile = new File("C:/projects/Java/Epam_UpSkill/tasks/module1_3/SortingApp" +
//                    "/SortingApp/src/tes/resources/testInputNormalCase.txt");
            Scanner in = new Scanner(inputFile);
            try {
                for(run=0; run<5; run++) {
                    inp = in.nextLine();
                    check[run]=inp.split(",");
                }
            }
            catch(NoSuchElementException e) {System.out.println("End of file");};
        } catch(Exception ex){ex.printStackTrace();}
        return Arrays.asList(check);
    }

    @Test
    public void sortingWorkingCaseTest() {
        assertThat(Sorting.sort(input), is(expected));
    }



}