package com.alkhr;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
/**
 * Execute all tests
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SortingEmptyTest.class,
        SortingWorkingCaseTest.class,
        SortingMoreThenTenCaseTest.class,
        SortingInvalidInputCaseTest.class
})
public class SortingTest {

}
