package com.alkhr;

import org.junit.Test;

/**
 * Check empty input cases
 */
public class SortingEmptyTest {
    @Test(expected = IllegalArgumentException.class)
    public void whitespaceTest() {
        Sorting.sort("     ");
    }
    @Test(expected = IllegalArgumentException.class)
    public void emptyTest() {
        Sorting.sort("");
    }

}