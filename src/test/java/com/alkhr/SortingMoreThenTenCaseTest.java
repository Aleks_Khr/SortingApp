package com.alkhr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.net.URL;
import java.util.*;



/**
 * Check more than ten numbers input cases
 */

@RunWith(Parameterized.class)
public class SortingMoreThenTenCaseTest {


    String input, expected;

    public SortingMoreThenTenCaseTest(String input, String expected) {
        this.input=input;
        this.expected=expected;
    }
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        String[] inArr = new String[2];
        String inp;
        int run=5;
        Object[][] check = new Object[run][2];
        try {
            URL url = SortingInvalidInputCaseTest.class.getClass().getResource("/testInputLongCase.txt");
            File inputFile = new File(url.getPath());
//            File inputFile = new File("C:/projects/Java/Epam_UpSkill/tasks/module1_3/SortingApp" +
//                    "/SortingApp/src/test/resources/testInputLongCase.txt");
            Scanner in = new Scanner(inputFile);
            try {


                for(run=0; run<5; run++) {
                    inp = in.nextLine();
                    check[run]=inp.split(",");
                }
            }
            catch(NoSuchElementException e) {System.out.println("End of file");};
        } catch(Exception ex){ex.printStackTrace();}
        return Arrays.asList(check);

    }

        @Test(expected = IllegalArgumentException.class)
    public void sortingMoreThenTenCaseTest() {
        Sorting.sort(input);
           //assertThat(Sorting.sort(input), is(expected));
    }


}