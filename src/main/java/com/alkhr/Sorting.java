package com.alkhr;

import java.util.Arrays;


/**
 * Class Sorting contains methods for quicksort algorithm implementation.
 */
public class Sorting {
    /**
     * Sort method, invokes sorting
     * @param input String of integer values separated by whitespaces
     * @return String of integer values sorted in the ascending order
     * @throws IllegalArgumentException if invalid input
     */
    static public String sort(String input) throws IllegalArgumentException{
         String output;
         if(input.length()==0) {
             throw new IllegalArgumentException();
         }
        input=input.trim().replaceAll("\\s+", " ");
        input=input.replaceAll("\\s+", " ");

        String[] inputArr = input.split(" ");
        if(inputArr.length>10) {output = "More then 10 numbers unsupported"; throw new IllegalArgumentException();}
        if((input.replace(" ","")).length()==0) {output = "Empty input"; throw new IllegalArgumentException();}
        else {
            for(int n=0; n<inputArr.length; n++) {
                if(!inputArr[n].matches("[+-]?\\d+")) {output = "Integer values supported only"; throw new IllegalArgumentException();}//return output;}//.?[0-9]*")) {break;}
            }
                int[] inputIntArr = new int[inputArr.length];
                for(int i=0; i<inputArr.length; i++) {
                    if(inputArr[i].length()>9) {output = "Integer values supported only"; throw new IllegalArgumentException();}
                    else { inputIntArr[i] = Integer.parseInt(inputArr[i]); }
                }
            quicksort(inputIntArr, 0, inputIntArr.length - 1);
            output= Arrays.toString(inputIntArr);
            output=output.replace(",", "");
            output=output.replace("[", "");
            output=output.replace("]", "");

        }
    return output;
    }

    /**
     *quicksort method recursively invokes arrays splitting by two with key-value
     * @param array input array
     * @param less starting element index in less numbers array
     * @param bigger finish element index in greater numbers array
     */

    static public void quicksort(int array[], int less, int bigger) {

        if (less < bigger) {
            int key = split(array, less, bigger);
            quicksort(array, less, key - 1);
            quicksort(array, key + 1, bigger);
        }
    }

    /**
     *This method split array by two: less and bigger then key-value
     * @param array array for sorting
     * @param less less index
     * @param bigger bigger index
     * @return key-value
     */

    static public int split(int array[], int less, int bigger) {
        int key = array[bigger];
        int i = less - 1;
        int t;
        for (int j = less; j < bigger; j++) {
            if (array[j] < key) {
                i++;
                t = array[i];
                array[i] = array[j];
                array[j] = t;
            }
        }
        t = array[i + 1];
        array[i + 1] = array[bigger];
        array[bigger] = t;
        return (i + 1);
    }

}
