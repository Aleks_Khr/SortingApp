package com.alkhr;



import java.util.Scanner;

/**
 * Class for user input/output by command line (simple sorting app, takes up to ten command-line arguments as integer values and sorts them in the ascending order).
 */
public class App
{
    /**
     * Method contains user input reading and output sorted string by command line.
     * @param args command-line arguments.
     */
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input up to ten integer values");
        String input = scanner.nextLine();
        System.out.println(Sorting.sort(input));
    }
}

