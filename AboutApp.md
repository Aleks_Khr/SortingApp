* This application takes up to ten command-line arguments as integer values, sorts them and output in the ascending order
* In case of incorrect input application throws IllegalArgumentException
* In project included parametrized unit tests that takes values from files stored in resources folder
* Java doc created and added in the project folder /javaDoc/index.html